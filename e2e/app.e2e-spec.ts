import { RxBetcatPage } from './app.po';

describe('rx-betcat App', () => {
  let page: RxBetcatPage;

  beforeEach(() => {
    page = new RxBetcatPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
