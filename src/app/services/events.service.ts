import { Injectable } from '@angular/core';

import { CompetitionsService, ICompetition } from './competitions.service';
import { DatesService, IDate } from './dates.service';
import { IEvent } from './events.service';
import { Observable } from '../app.rx';
import { groupBy, sortBy } from './services.helpers';

export interface IEvent {
    id: number;
    title: string;
    competitionId: number;
    date: string;
}

export interface IEventsByDate {
    date: string;
    events: IEvent[];
}

export interface IEventsByCompetition {
    competitionId: number;
    events: IEvent[];
}

export interface IEventsByDateCompetition {
    date: string;
    competitionEvents: IEventsByCompetition[];
}

@Injectable()
export class EventsService {

  public events$: Observable<IEvent[]>;
  public eventsByDate$: Observable<IEventsByDate[]>;
  public visibleEventsByDate$: Observable<IEventsByDate[]>;
  public visibleEventsByDateByCompetition$: Observable<IEventsByDateCompetition[]>;
  public eventDates$: Observable<string[]>;

  /**
   * Pull in the competitions, dates services through DI
   */
  constructor(
    private _competitionsService: CompetitionsService,
    private _datesService: DatesService) {

      this.eventsByDate$ =
        this._competitionsService.competitionEventsViaActions$
          .flatMap((events: IEvent[]) => {
            const groupedEvents = groupBy(events, (e) => e.date);
            const arrayOfGroups = [];

            groupedEvents.forEach((eventForDate, key) => {
              arrayOfGroups.push({
                date: key,
                events: eventForDate,
              });
            });
            return Observable.from([arrayOfGroups]);
          });


      this.visibleEventsByDate$ =
        Observable.combineLatest(
          this.eventsByDate$,
          this._datesService.selectedDates$,
          (eventsByDate: IEventsByDate[], selectedDates: IDate[]) => {
            /**
             * return events filtered by selected dates, ordered by event date
             */
            return eventsByDate.filter(({date}) => {
              return selectedDates
                  .find((d: IDate) => d.date === date);
              })
              .sort(sortBy('date'));
          });

      this.visibleEventsByDateByCompetition$ =
        this.visibleEventsByDate$
          .map((eventsByDate: IEventsByDate[]) => {
            return eventsByDate.map(dateEvents => {
              // group date events by competition within the date grouping
              const groupedEvents = groupBy(dateEvents.events, (e) => e.competitionId);
              const arrayOfGroups = [];

              groupedEvents.forEach((eventsForCompetition, key) => {
                arrayOfGroups.push({
                  competitionId: key,
                  events: eventsForCompetition,
                });
              });

              return {
                date: dateEvents.date,
                competitionEvents: arrayOfGroups,
              };
            });
          });

      /**
       * The dates the selected competition events occur on, ie the ones you can choose from.
       */
      this.eventDates$ =
        this._competitionsService.competitionEventsViaActions$
          .map((events: IEvent[]) => {
            return Array.from(groupBy(events, (e) => e.date).keys());
          });
  }
}
