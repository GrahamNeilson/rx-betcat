import { Injectable } from '@angular/core';

import { Observable, Subject } from '../app.rx';
import { CompetitionsService } from './competitions.service';
import { SportsService } from './sports.service';
import { generateInitialDates } from './services.helpers';

export interface IDate {
  id: number;
  date: string;
  selected: boolean;
  disabled: boolean;
}

@Injectable()
export class DatesService {
  /**
   * This is a subject in order to listen to new date selections
   * A subject is acting as an event bus in this case
   * We're using subjects to transform user input into an event stream
   */
  public dateToggle$: Subject<IDate[]> = new Subject();

  /**
   * resets every change of sport, which re-seeds the scan in selected dates
   */
  public dates$: Observable<IDate[]>;
  public selectedDates$: Observable<IDate[]>;

  /**
   * Pull in the events service via dependency injection
   */
  constructor(
    private _sportsService: SportsService,
    private _competitionsService: CompetitionsService,
  ) {
    /**
     * resets every change of sport, which re-seeds the scan in selected dates
     */
    this.dates$ =
      this._sportsService
        .sportSelection$
        .map(s => {
          return generateInitialDates();
        })
        .startWith(generateInitialDates());

    /**
     * this could be remaned dates state or something really
     */
    this.selectedDates$ =
      this.dates$
        .mergeMap((seedDates: IDate[]) =>
          this.dateToggle$
            .startWith({})
            .scan((current: IDate[], newSelection: IDate) => {
              // toggle selected any passed date, otherwise return the previous value
              return current.map((date: IDate) => {
                if (date.id === newSelection.id) {
                  date.selected = !date.selected;
                }
                return date;
              });
            }, seedDates)
        )
        .map((dates:IDate[]) => dates.filter(date => date.selected));

  }
}
