import { IDate } from './dates.service';

export function groupBy(list, keyGetter) {
  const map = new Map();
  list.forEach((item) => {
    const key = keyGetter(item);
    if (!map.has(key)) {
      map.set(key, [item]);
    } else {
      map.get(key).push(item);
    }
  });
  return map;
}

export const sortBy = (key: string) => {
  return (a: any, b: any) => {
    if (a[key] > b[key]) {
      return 1;
    }
    if (a[key] < b[key]) {
      return -1;
    }
    return 0;
  };
};

/**
 * just quickly generate the next week's dates
 */
export function generateInitialDates(startDate = new Date(), daysToAdd = 6): IDate[] {
  const dates: IDate[] = [];

  for (let i = 0; i <= daysToAdd; i++) {
    const date = new Date();
    date.setDate(startDate.getDate() + i);
    // by default, as per our app, start with sat and sunday 'on'
    const selected = date.getDay() === 0 || date.getDay() === 6;

    dates.push({
      id: i + 1,
      date: date.toDateString(),
      selected,
      disabled: false,
    });
  }

  return dates;
}
