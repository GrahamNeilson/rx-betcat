import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { SportsService } from './sports.service';
import { IEvent } from './events.service';
import { Observable, Subject } from '../app.rx';

export interface ICompetition {
  id: number;
  title: string;
  sportId: number;
  selected?: boolean;
  events?: IEvent[];
  eventCount?: number;
}

interface IAction {
  type: string;
  payload: any;
}

@Injectable()
export class CompetitionsService {

  /**
   * Subject, in order to easily accept new values from UI
   * Although a subject, the Observable looks like:
   * .....a......b....c.....c.......a.....a............
   *
   * where a, b, c are 'competitions toggles'
   * a competition toggle is a competition object, with current on/off state 'selected'
   */
  public competitionToggle$: Subject<ICompetition> = new Subject();

  /**
   * ......sc1..........sc2.............sc3.............
   * ......c............................c...............
   *
   * sc1 thru sc3 are arrays of sport competitions
   * sport sc2 has no competitions
   * c is a competition
   * When a sport changes, we toggle the first competition on by default
   * Using the first by default is just an example
   * Could come from server, or more likely from a previous visit
   */
  private _competitionToggleFromSportChange$: Observable<ICompetition> =
    this._sportsService
      .sportCompetitions$
      .do(() => console.log('competition toggle from sport change')) // do is like tap in Ramda - debug, pass value on
      .filter(comps => comps.length > 0) // ensure we have a first
      .map(comps => comps[0]); // return first

  /**
   * .........c.......................c................. competitionToggle$
   * ...........c..............c........................ competitionToggleFromSportChange$
   *
   * .........c.c..............c......c.................
   *
   * Where c is a competition, selected or not selected.
   * I've re-used the name as the name makes sense, just note it's _private
   *
   * refCount() returns an observable
   */
  private _competitionToggle$: Observable<ICompetition> =
    Observable
      .merge(
        this._competitionToggleFromSportChange$, // map to 1st competition
        this.competitionToggle$)
      .do(() => console.log('merging sport change comp toggle with checkbox toggles'))
      .publish()
      .refCount();


  /**
   * .........c1................c2...........c3..........
   * .........c..............................c...........
   *
   * c2 has property selected = true.
   */
  private _competitionRequest$: Observable<ICompetition> =
    this._competitionToggle$
      .filter((competition: ICompetition) => !competition.selected);

  /**
   * .........c1................c2...........c3..........
   * ...........................c........................
   *
   * c2 has property selected = true, ie it's on, we're toggling off.
   */
  private _competitionDismissal$: Observable<ICompetition> =
    this._competitionToggle$
      .filter((competition: ICompetition) => competition.selected);

  /**
   * The competitions, merged with the current state of each - on/off
   * ..........ct.........ct...............ct....
   * ..........c..........c................c.....
   */
  public menuCompetitions$: Observable<ICompetition[]> =
    Observable
      .combineLatest(
        this._competitionToggle$,
        this._sportsService.sportCompetitions$,
        (toggledCompetition, competitions) => {

          return competitions.map(competition => {
              if (competition.id === toggledCompetition.id) {
                competition.selected = !competition.selected;
              }
              return competition;
            });
          }
        )
        .publish()
        .refCount();

  /**
   * ...........c..............c..............c.....
   * where c is a competition with data from server.
   */
  public completedCompetitionRequest$: Observable<ICompetition> =
    this._competitionRequest$
      .mergeMap(competition => {
        /**
         * This is required by the 'stop' observable below.
         * An observable that next's a toggle off for this competition.
         *
         * .........c1............c2.............c3..... _competitionDismissal$
         * ......................................c3..... toggledOff$ for this competition
         * Where this competition is c3, say
         * Has to be here, within the outer obervable _completedCompetitionRequest, as we
         * need the competition id in scope.
         */
        const toggledOff$ =
          this._competitionDismissal$
            .filter(c => c.id === competition.id);

        /**
         * Create  a 'stop' observable based either a toggle off or change of sport
         * "We're not interested in the result of this http call anymore, or even making it at all (where poss)"
         *
         * .......c...c...........c............... toggledOff
         * ................s................s..... sportSelection
         * .......c...c....s......c.........s..... stop
         */
        const stop$ =
          Observable.merge(
            toggledOff$,
            this._sportsService.sportSelection$
          );

        /**
         * if call completes before stop$ fires
         * ................c|          http call
         * ........................... stop$, at some point after the call has ended, or never
         * ................c|          result
         *
         * if call does not complete before stop$
         * * .......................c| http call
         * .................s|         stop$, at some point before the call has ended.
         * ..................|         result
         */
        return this._http
          .get(`http://localhost:4040/competitions/${competition.id}`)
          .retry(3)
          .map((res: Response) => res.json())
          .map(comp => comp ? comp : {}) // tidy this and the api end point
          .takeUntil(stop$);
        })
        .publish()
        .refCount();

  private _dismissAllCompetitions$: Observable<Object> =
    this._sportsService.sportSelection$.mapTo({id: 0 });

  /**
   * ...............c.................c.............c...... _completedCompetitionRequest$, id: id, events: []
   * ........d................d............................ _competitionDismissal$, id: id, no events
   * .....................................dA............... _dismissAllCompetitions$: id: 0
   *
   * ........d......c.........d.......c...dA........c...... scan this to build selected comps
   *
   * this merged result feeds new comps, comps to delete or a delete all flag into, effetivley, a reducer
   */
  public competition$ =
    Observable.merge(
      this.completedCompetitionRequest$,
      this._competitionDismissal$,
      this._dismissAllCompetitions$ // acts as a 'delete all' dismissal, whatever we pass should satisfy ICompetition
    );


  /**
   * cA1 means comp addition of id 1, cD means dismiss
   * ..........cA1.............cA2.........cD1.........cA3.........
   * ..........[c1]............[c1, c2]....[c2]........[c2, c3]....
   *
   * this is the logic to keep the 'store' up to date, loosely speaking.
   * it also dismisses all events when the competition id is 0
   */
  public competitionEvents$ =
    this.competition$
      .scan((events: IEvent[], competition: ICompetition) => {
        // now, this should / could look like a redux reducer
        // see below, have implemeted as an example: competitionEventsViaActions

        return competition.events ?
          // addition of a new competition's events
          [...events, ...competition.events] :
          // deletion of competition events, or delete all
          events.filter(e => {
            // id 0 filters out all (on change of sport)
            // otherwise, filer out only the dismissal
            return competition.id === 0 ?
              false : e.competitionId !== competition.id;
          });
      }, []);

  /**
   * REDUX style implementaion of above 2 observables, for clarity / comparison
   * First, map that stream to a more usable stream of actions
   */
  public competitionAction$: Observable<IAction> =
    this.competition$.map((competitionAction: any) => {
      if (competitionAction.events) {
        return {
          type: 'ADD_COMPETITION_EVENTS',
          payload: {
            events: competitionAction.events,
          }
        };
      }

      if (competitionAction.id === 0) {
        return {
          type: 'DELETE_ALL_EVENTS',
          payload: {
            events: competitionAction.events,
          }
        };
      }

      return {
        type: 'DELETE_COMPETITION_EVENTS',
        payload: {
          competitionId: competitionAction.id
        }
      };
    });

  public competitionEventsViaActions$: Observable<IEvent[]> =
    this.competitionAction$
      .scan((events: IEvent[], action: IAction) => {
        switch (action.type) {
          case 'ADD_COMPETITION_EVENTS':
            return [...events, ...action.payload.events];
          case 'DELETE_ALL_EVENTS':
            return [];
          case 'DELETE_COMPETITION_EVENTS':
            return events.filter(e => e.competitionId !== action.payload.competitionId);
        }
      }, []);

  /**
   * Pull in the http and sports services via dependency injection
   */
  constructor(
    private _sportsService: SportsService,
    private _http: Http
  ) {};
}
