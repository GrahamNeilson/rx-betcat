import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { Observable, Subject } from '../app.rx';
import { ICompetition } from './competitions.service';

/**
 * Interfaces feel essential for debugging - types really help when composing, and where you
 * are in terms of flattening / depth too.
 */
export interface ISport {
    id: number;
    title: string;
}

/**
 * This is just a class.
 * The angular @Injectable decorator essentially means it's a singleton available for DI
 */
@Injectable()
export class SportsService {

  public sportClick$;
  public sportSelection$: Observable<ISport>;
  public sports$: Observable<ISport[]>;
  public sportCompetitions$: Observable<ICompetition[]>;

  /**
   * Pull in the http service via dependency injection
   */
  constructor(private _http: Http) {
    /**
     * Subject, in order to easily accept new values from UI
     * Although a subject, the Observable looks like:
     * .....f......t....t.....c.......b.....tt.......t......
     *
     * where f, t, c, etc are sports
     */
    this.sportClick$ = new Subject();


    /**
     * Distinct request for sports
     * ie a request for new sports data
     * Using the above sportClick marble, the observable would look like:
     * .....f......t..........c.......b.....t...............
     */
    this.sportSelection$ =
      this.sportClick$
        .distinctUntilChanged();

    /**
     * All sports from server
     * .....s|
     * where s is an array of sports
     */
    this.sports$ =
      this._http
        .get('http://localhost:4040/sports')
        .retry(3)
        .map((res: Response) => res.json());

    /**
     * Comptitions for the current sport choice
     * startWith 1, i.e. immediate, hence no preceding '...' on sportSelection$ mard.
     *
     * s....................s....s.................... sportSelection$
     * |........c|          |    |                     http call
     *                      |....|.......c|            slow http call, ignored due to switch latest
     *                           |............c|       another http call after quick change of sports (switchLatest)
     * ...........c...........................c....... sportCompetitions$
     *
     */
    this.sportCompetitions$ =
      this.sportSelection$
        .map((sport: ISport) => sport.id) // pluck id from selected sport object
        .startWith(1) // start with football as a seed value
        .switchMap((id: number) =>  // aka switch latest / flat map - flatten, basically
          this._http
            .get(`http://localhost:4040/sports/${id}/competitions`)
            .retry(3) // or more sophisticated error handling as per bet cat MR demo
            .map((res: Response) => res.json())
        )
        .publish()
        .refCount();

  }
}
