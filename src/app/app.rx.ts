// import 'rxjs/Rx'; // adds ALL RxJS statics & operators to Observable
// See node_module/rxjs/Rxjs.js
// Import just the rxjs statics and operators we need for THIS app.
// Then import from this file within the app.

// direct exports
export { Observable } from 'rxjs/Observable';
export { Subscription } from 'rxjs/Subscription';

export { Subject  } from 'rxjs/Subject';
export { ReplaySubject } from 'rxjs/ReplaySubject';
export { BehaviorSubject } from 'rxjs/BehaviorSubject';
export { TestScheduler } from 'rxjs/testing/TestScheduler';

// export { Subscription } from 'rxjs/Subscription';
// export { WebSocketSubject } from 'rxjs/observable/dom/WebSocketSubject';

// static methods
import 'rxjs/add/observable/timer';
import 'rxjs/add/observable/empty';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/range';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/combineLatest';

// operators
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/concat';
import 'rxjs/add/operator/scan';
import 'rxjs/add/operator/groupBy';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mapTo';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/retryWhen';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/publish';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/multicast';
import 'rxjs/add/operator/reduce';
import 'rxjs/add/operator/withLatestFrom';
