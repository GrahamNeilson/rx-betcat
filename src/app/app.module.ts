import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {
  CalendarComponent,
  CatalogComponent,
  CompetitionEventsComponent,
  CompetitionToggleComponent,
  CompetitionsMenuComponent,
  DateEventsComponent,
  DateToggleComponent,
  EventComponent,
  EventsComponent,
  FilterManagerComponent,
  SportLinkComponent,
  SportMenuComponent,
} from './catalog';

import {
  SportsService,
  CompetitionsService,
  DatesService,
  EventsService,
} from './services';

@NgModule({
  declarations: [
    AppComponent,
    CalendarComponent,
    CatalogComponent,
    CompetitionEventsComponent,
    CompetitionToggleComponent,
    CompetitionsMenuComponent,
    DateEventsComponent,
    DateToggleComponent,
    EventComponent,
    EventsComponent,
    FilterManagerComponent,
    SportLinkComponent,
    SportMenuComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [
    SportsService,
    CompetitionsService,
    DatesService,
    EventsService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
