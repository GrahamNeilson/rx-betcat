import { Component, Input, Output, EventEmitter } from '@angular/core';

/**
 * Dumb Component
 */
@Component({
  selector: `app-date-toggle`,
  template: `
    <div>
      <label>
        <input type="checkbox"
          (click)="toggle.emit(date)"
          [checked]="date.selected"
          [disabled]="date.disabled">
        <span *ngIf="!date.disabled">
          {{date.date}}
        </span>
        <span *ngIf="date.disabled">
          Not available
        </span>
      </label>
    </div>
  `,
})
export class DateToggleComponent {
  @Input()
  date;

  @Output()
  toggle = new EventEmitter();
}
