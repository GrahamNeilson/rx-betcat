import { Component, OnInit } from '@angular/core';

import { DatesService, IDate } from '../services/dates.service';
import { SportsService, ISport } from '../services/sports.service';
import { CompetitionsService, ICompetition } from '../services/competitions.service';
import { EventsService } from '../services/events.service';
import { Observable } from '../app.rx';

@Component({
  selector: `app-filter-manager`,
  templateUrl: 'filter-manager.component.html',
})

export class FilterManagerComponent implements OnInit {

  calendarDates$: Observable<IDate[]>;
  sports$: Observable<ISport[]>;
  competitions$: Observable<ICompetition[]>;

  constructor(
    private _competitionsService: CompetitionsService,
    private _datesService: DatesService,
    private _eventsService: EventsService,
    private _sportsService: SportsService) {};

  ngOnInit() {
    this.calendarDates$ =
      Observable.combineLatest(
        this._datesService.dates$,
        this._eventsService.eventDates$,
        (dates: IDate[], eventDates: string[]) => {
          return dates.map(date => {
            return !eventDates.includes(date.date) ?
              Object.assign({}, date, {disabled: true}) : date;
          });
        }
      );

    this.sports$ = this._sportsService.sports$;
    this.competitions$ = this._competitionsService.menuCompetitions$;
  }

  handleDateToggle(date) {
    this._datesService
      .dateToggle$.next(Object.assign({}, date));
  }

  handleCompetitionToggle(competition) {
    this._competitionsService
      .competitionToggle$.next(Object.assign({}, competition));
  }

  handleSportClick(sport) {
    this._sportsService
      .sportClick$.next(sport);
  }
}
