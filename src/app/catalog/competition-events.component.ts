import { Component, Input } from '@angular/core';

@Component({
  selector: `app-competition-events`,
  template: `
    <h4>Competition events {{competitionEvents.competitionId}}</h4>
    <div *ngFor="let event of competitionEvents.events">
      <app-event [event]="event"></app-event>
    </div>
  `,
})
export class CompetitionEventsComponent  {
  @Input()
  competitionEvents;
}
