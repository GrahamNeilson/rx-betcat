import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: `app-sport-link`,
  template: `
    <div (click)="sportClick.emit(sport)">
      {{sport.title}}
    </div>
  `,
})
export class SportLinkComponent {
  @Input()
  sport;

  @Output()
  sportClick = new EventEmitter();
}
