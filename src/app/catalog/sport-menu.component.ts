import { Component, Input, Output, EventEmitter } from '@angular/core';

import { ISport } from '../services/sports.service';

@Component({
  selector: `app-sports-menu`,
  template: `
    <ul>
      <li *ngFor="let sport of sports">
        <app-sport-link
          [sport]="sport"
          (sportClick)="sportClick.emit($event)">
        </app-sport-link>
      </li>
    </ul>
  `,
})
export class SportMenuComponent {
  @Input()
  sports: ISport[];

  @Output()
  sportClick = new EventEmitter();
}
