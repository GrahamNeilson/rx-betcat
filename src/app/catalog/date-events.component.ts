import { Component, Input } from '@angular/core';

@Component({
  selector: `app-date-events`,
  template: `
    <h4>Date events {{dateEvents.date}}</h4>

    <div *ngFor="let competitionEvents of dateEvents.competitionEvents">
      <app-competition-events [competitionEvents]="competitionEvents"></app-competition-events>
    </div>
  `,
})
export class DateEventsComponent  {
  @Input()
  dateEvents;

  constructor() {}
}
