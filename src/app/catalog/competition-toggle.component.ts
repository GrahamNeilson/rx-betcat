import { Component, Input, Output, EventEmitter } from '@angular/core';

/**
 * Dumb Component
 */
@Component({
  selector: `app-competition-toggle`,
  templateUrl: 'competition-toggle.component.html',
})
export class CompetitionToggleComponent {
  @Input()
  competition;

  @Output()
  toggle = new EventEmitter();
}
