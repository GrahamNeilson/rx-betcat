import { Component } from '@angular/core';

/**
 * container for filters and events / events list
 */
@Component({
  selector: `app-catalog`,
  template: `
    <app-filter-manager></app-filter-manager>
    <app-events></app-events>
  `,
})
export class CatalogComponent {}
