import { Component, Input, Output, EventEmitter } from '@angular/core';

import { IDate } from '../services/dates.service';

/**
 * Dumb component
 */
@Component({
  selector: `app-calendar`,
  templateUrl: './calendar.component.html',
})
export class CalendarComponent {
  @Input()
  dates: IDate[];

  @Output()
  toggle = new EventEmitter();
}
