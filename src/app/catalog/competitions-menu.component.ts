import { Component, Input, Output, EventEmitter } from '@angular/core';

import { ICompetition } from '../services/competitions.service';

/**
 * Dumb component
 */
@Component({
  selector: `app-competitions-menu`,
  template: `
    <ul>
      <li *ngFor="let competition of competitions">
        <app-competition-toggle
          [competition]="competition"
          (toggle)="toggle.emit($event)">
        </app-competition-toggle>
      </li>
    </ul>
  `,
})
export class CompetitionsMenuComponent {
  @Input()
  competitions: ICompetition[];

  @Output()
  toggle = new EventEmitter();
}
