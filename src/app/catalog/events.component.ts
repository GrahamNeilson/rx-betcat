import { Component } from '@angular/core';

import { Observable } from '../app.rx';
import { EventsService, IEventsByDateCompetition } from '../services/events.service';
import { DatesService  } from '../services/dates.service';

@Component({
  selector: `app-events`,
  template: `
    <h4>Selected dates' events</h4>
    <div *ngFor="let eventDate of visibleEventsByDateByCompetition$ | async">
      <app-date-events [dateEvents]="eventDate"></app-date-events>
    </div>
  `,
})
export class EventsComponent  {
  visibleEventsByDateByCompetition$: Observable<IEventsByDateCompetition[]>;

  constructor(
    private _eventsService: EventsService,
    private _datesService: DatesService
  ) {
    this.visibleEventsByDateByCompetition$ = this._eventsService.visibleEventsByDateByCompetition$;
  }
}
