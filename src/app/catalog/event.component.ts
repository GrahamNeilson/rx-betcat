import { Component, Input, OnInit, OnDestroy} from '@angular/core';
import { IEvent,  } from '../services/events.service';

@Component({
  selector: `app-event`,
  template: `
    <h6>{{event.title}}</h6>

    <div *ngIf="event.markets">
      Markets
    </div>

    <div *ngIf="!event.markets">
      Loading markets....
    </div>
  `,
})

export class EventComponent implements OnInit, OnDestroy {

  @Input()
  event = <IEvent>null; // until angular ugrade to handle weird multiple interface export issue
  // (https://github.com/angular/angular-cli/issues/2034)

  ngOnInit() {
    console.log(`Init event ${this.event.title} &  markets & subs`);
  }

  ngOnDestroy() {
    console.log(`Destroy event ${this.event.title} & markets & subs`);
  }
}
